# Mangirdas Mailbox

Simple Mailbox API

## Setup instructions

- Clone this repository
- Make sure you have PHP >=7.0 installed
- Make sure MySql server is installed and running
- execute `composer install` (and enter database credentials)
- execute `php bin/console doctrine:database:create`
- execute `php bin/console doctrine:schema:update --force`
- execute `php bin/console app:import-messages messages_sample.json`
- execute `php bin/console server:run {your-ip}:8000`
- open the root of the website http://{your-ip}:8000
- enter admin / admin as username and password
- you will see a link to the API documentation

## Precautions

- tested on Unix environment only

## Testing

- enter test database credentials to app/config/parameters_test.yml file
- execute `php bin/console --env=test doctrine:database:create`
- execute `php bin/console --env=test doctrine:schema:update --force`
- execute `php vendor/bin/simple-phpunit`
