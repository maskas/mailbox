<?php

namespace Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpKernel\Client;

class MailboxTestCase extends WebTestCase
{

    /** @var  Client */
    private $client;

    /**
     * Constructs a test case with the given name.
     *
     * @param string $name
     * @param array  $data
     * @param string $dataName
     */
    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $this->client = static::createClient(array(), array(
            'PHP_AUTH_USER' => 'admin', //hard-coding is not cool
            'PHP_AUTH_PW'   => 'admin',
        ));
    }

    /**
     * @return Client
     */
    public function getClient(): Client
    {
        return $this->client;
    }

}
