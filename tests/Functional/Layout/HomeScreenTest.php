<?php

namespace Tests\Functional\Layout;

use Tests\MailboxTestCase;

class HomeScreenTest extends MailboxTestCase
{
    public function testIndex()
    {
        $client = $this->getClient();
        $crawler = $client->request('GET', '/');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains('Mangirdas Mailbox API', $crawler->filter('h1')->text());
    }
}
