<?php

namespace Tests\Functional\Layout;

use Tests\MailboxTestCase;

class DocumentationTest extends MailboxTestCase
{
    public function testIndex()
    {
        $client = $this->getClient();
        $crawler = $client->request('GET', '/api/doc');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertEquals('API documentation', $crawler->filter('h1')->text());
    }
}
