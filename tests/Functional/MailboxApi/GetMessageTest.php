<?php

namespace Tests\Functional\MailboxApi;

use Doctrine\Common\DataFixtures\Executor\ORMExecutor;
use Doctrine\Common\DataFixtures\Loader;
use Doctrine\Common\DataFixtures\Purger\ORMPurger;
use Tests\Functional\MailboxApi\DataFixture\GetMessageFixture;
use Tests\MailboxDbTestCase;

class GetMessageTest extends MailboxDbTestCase
{

    public function setUp()
    {
        $purger = new ORMPurger();
        $executor = new ORMExecutor($this->getEntityManager(), $purger);

        $loader = new Loader();
        $loader->addFixture(new GetMessageFixture());

        $executor->execute($loader->getFixtures());
    }

    public function testGet()
    {
        $client = $this->getClient();
        $client->request('GET', '/api/message/100');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $responseData = json_decode($client->getResponse()->getContent(), true);
        $this->assertEquals(100, $responseData['id']);
        $this->assertEquals('jon@example.com', $responseData['sender']);
        $this->assertEquals('subjectus', $responseData['subject']);
        $this->assertEquals('messagus', $responseData['message']);
        $this->assertEquals(false, $responseData['is_archived']);
        $this->assertEquals(false, $responseData['is_read']);
        $this->assertEquals(1500000000, $responseData['time_sent']);
    }
}
