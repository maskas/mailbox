<?php

namespace Tests\Functional\MailboxApi;

use Doctrine\Common\DataFixtures\Executor\ORMExecutor;
use Doctrine\Common\DataFixtures\Loader;
use Doctrine\Common\DataFixtures\Purger\ORMPurger;
use Tests\Functional\MailboxApi\DataFixture\GetMessagesContentFixture;
use Tests\MailboxDbTestCase;

class GetMessagesContentTest extends MailboxDbTestCase
{
    public function setUp()
    {
        $purger = new ORMPurger();
        $executor = new ORMExecutor($this->getEntityManager(), $purger);

        $loader = new Loader();
        $loader->addFixture(new GetMessagesContentFixture());

        $executor->execute($loader->getFixtures());
    }

    public function testFields()
    {
        $client = $this->getClient();
        $client->request('GET', '/api/message');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $responseData = json_decode($client->getResponse()->getContent(), true);
        $this->assertArrayHasKey(0, $responseData);
        $firstMessage = $responseData[0];
        $this->assertEquals(1, $firstMessage['id']);
        $this->assertEquals('jon@example.com', $firstMessage['sender']);
        $this->assertEquals('subjectus', $firstMessage['subject']);
        $this->assertEquals('messagus', $firstMessage['message']);
        $this->assertEquals(false, $firstMessage['is_read']);
        $this->assertEquals(1800000000, $firstMessage['time_sent']);

        //we are listing only non-archived messages. Why would we need this field for?
        $this->assertArrayNotHasKey('is_archived', $firstMessage);
    }

}
