<?php

namespace Tests\Functional\MailboxApi;

use Doctrine\Common\DataFixtures\Executor\ORMExecutor;
use Doctrine\Common\DataFixtures\Loader;
use Doctrine\Common\DataFixtures\Purger\ORMPurger;
use Tests\Functional\MailboxApi\DataFixture\ListMessagesFixture;
use Tests\MailboxDbTestCase;

class GetMessagesPaginationTest extends MailboxDbTestCase
{
    const DEFAULT_ITEM_LIMIT = 3;

    public function setUp()
    {
        $purger = new ORMPurger();
        $executor = new ORMExecutor($this->getEntityManager(), $purger);

        $loader = new Loader();
        $loader->addFixture(new ListMessagesFixture());

        $executor->execute($loader->getFixtures());
    }

    public function testDefaultItemLimitIs3()
    {
        $client = $this->getClient();
        $client->request('GET', '/api/message');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $responseData = json_decode($client->getResponse()->getContent(), true);
        $this->assertEquals(self::DEFAULT_ITEM_LIMIT, count($responseData));
    }

    public function testItemLimitIsWorking()
    {
        $itemLimit = 5;
        $client = $this->getClient();
        $client->request('GET', '/api/message', ['limit' => $itemLimit]);

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $responseData = json_decode($client->getResponse()->getContent(), true);
        $this->assertEquals($itemLimit, count($responseData));
    }

    public function testItemLimitIsCustomizable()
    {
        $itemLimit = 5;
        $client = $this->getClient();
        $client->request('GET', '/api/message', ['limit' => $itemLimit]);

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $responseData = json_decode($client->getResponse()->getContent(), true);
        $this->assertEquals($itemLimit, count($responseData));
    }

    public function testPageIsCustomizable()
    {
        $page = 3;
        $expectedFirstItemId = ($page - 1) * self::DEFAULT_ITEM_LIMIT + 1;
        $client = $this->getClient();
        $client->request('GET', '/api/message', ['page' => $page]);

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $responseData = json_decode($client->getResponse()->getContent(), true);
        $this->assertArrayHasKey(0, $responseData);
        $this->assertEquals($expectedFirstItemId, $responseData[0]['id']);
    }

    public function testPartialResultIfNotEnoughItems()
    {
        $page = 4;
        $expectedResultCount = 1;
        $client = $this->getClient();
        $client->request('GET', '/api/message', ['page' => $page]);

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $responseData = json_decode($client->getResponse()->getContent(), true);
        $this->assertEquals($expectedResultCount, count($responseData));
    }

    public function testEmptyResultsOnNonExistingPage()
    {
        $page = 10000;
        $expectedResultCount = 0;
        $client = $this->getClient();
        $client->request('GET', '/api/message', ['page' => $page]);

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $responseData = json_decode($client->getResponse()->getContent(), true);
        $this->assertEquals($expectedResultCount, count($responseData));
    }

    public function testField()
    {
        $client = $this->getClient();
        $client->request('GET', '/api/message');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $responseData = json_decode($client->getResponse()->getContent(), true);
        $this->assertArrayHasKey(0, $responseData);
        $firstMessage = $responseData[0];
        $this->assertEquals(1, $firstMessage['id']);
        $this->assertEquals('jon@example.com1', $firstMessage['sender']);
        $this->assertEquals('subjectus1', $firstMessage['subject']);
        $this->assertEquals('messagus1', $firstMessage['message']);
        $this->assertEquals(false, $firstMessage['is_read']);
        $this->assertEquals(1800000000 - $firstMessage['id'], $firstMessage['time_sent']);

        //we are listing only non-archived messages. Why would we need this field for?
        $this->assertArrayNotHasKey('is_archived', $firstMessage);
    }

}
