<?php

namespace Tests\Functional\MailboxApi;

use Doctrine\Common\DataFixtures\Executor\ORMExecutor;
use Doctrine\Common\DataFixtures\Loader;
use Doctrine\Common\DataFixtures\Purger\ORMPurger;
use Tests\Functional\MailboxApi\DataFixture\GetArchivedMessagesSkipNonArchivedFixture;
use Tests\MailboxDbTestCase;

class GetArchivedMessagesSkipNonArchivedTest extends MailboxDbTestCase
{
    public function setUp()
    {
        $purger = new ORMPurger();
        $executor = new ORMExecutor($this->getEntityManager(), $purger);

        $loader = new Loader();
        $loader->addFixture(new GetArchivedMessagesSkipNonArchivedFixture());

        $executor->execute($loader->getFixtures());
    }

    public function testSkipNonArchived()
    {
        $client = $this->getClient();
        $client->request('GET', '/api/message-archived');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $responseData = json_decode($client->getResponse()->getContent(), true);
        $this->assertEquals(1, count($responseData));
        $this->assertEquals(2, $responseData[0]['id']);
    }
}
