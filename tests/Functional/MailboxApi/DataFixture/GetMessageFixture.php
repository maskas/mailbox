<?php

namespace Tests\Functional\MailboxApi\DataFixture;

use AppBundle\Entity\Message;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\ORM\Mapping\ClassMetadata;

class GetMessageFixture implements FixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $message = new Message(
            'jon@example.com',
            'subjectus',
            'messagus',
            1500000000
        );
        $message->setId(100);
        $metadata = $manager->getClassMetaData(Message::class);
        $metadata->setIdGeneratorType(ClassMetadata::GENERATOR_TYPE_NONE);
        $manager->persist($message);
        $manager->flush();
    }
}
