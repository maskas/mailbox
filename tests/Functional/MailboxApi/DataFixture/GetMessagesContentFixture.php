<?php

namespace Tests\Functional\MailboxApi\DataFixture;

use AppBundle\Entity\Message;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\ORM\Mapping\ClassMetadata;

class GetMessagesContentFixture implements FixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $message = new Message(
            'jon@example.com',
            'subjectus',
            'messagus',
            1800000000
        );
        $message->setId(1);
        $metadata = $manager->getClassMetaData(Message::class);
        $metadata->setIdGeneratorType(ClassMetadata::GENERATOR_TYPE_NONE);
        $manager->persist($message);
        $manager->flush();
    }
}
