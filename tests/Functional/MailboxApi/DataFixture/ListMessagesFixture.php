<?php

namespace Tests\Functional\MailboxApi\DataFixture;

use AppBundle\Entity\Message;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\ORM\Mapping\ClassMetadata;

class ListMessagesFixture implements FixtureInterface
{
    public function load(ObjectManager $manager)
    {
        for ($i=1; $i<=10; $i++) {
            $message = new Message(
                'jon@example.com' . $i,
                'subjectus' . $i,
                'messagus' . $i,
                1800000000 - $i
            );
            $message->setId($i);
            $manager->persist($message);
        }
        $metadata = $manager->getClassMetaData(Message::class);
        $metadata->setIdGeneratorType(ClassMetadata::GENERATOR_TYPE_NONE);
        $manager->flush();
    }
}
