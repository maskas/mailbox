<?php

namespace Tests\Functional\MailboxApi;

use Doctrine\Common\DataFixtures\Executor\ORMExecutor;
use Doctrine\Common\DataFixtures\Loader;
use Doctrine\Common\DataFixtures\Purger\ORMPurger;
use Tests\Functional\MailboxApi\DataFixture\MessageArchiveFixture;
use Tests\MailboxDbTestCase;

class MessageReadTest extends MailboxDbTestCase
{

    public function setUp()
    {
        $purger = new ORMPurger();
        $executor = new ORMExecutor($this->getEntityManager(), $purger);

        $loader = new Loader();
        $loader->addFixture(new MessageArchiveFixture());

        $executor->execute($loader->getFixtures());
    }

    public function testArchive()
    {
        $client = $this->getClient();
        $client->request('GET', '/api/message/1');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $responseData = json_decode($client->getResponse()->getContent(), true);
        $this->assertEquals(false, $responseData['is_read']);

        $client->request('PATCH', '/api/message/1/read');
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $responseData = json_decode($client->getResponse()->getContent(), true);
        $this->assertEquals(true, $responseData['is_read']);
    }
}
