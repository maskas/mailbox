<?php

namespace Tests\AppBundle\Importer;

use AppBundle\Entity\Message;
use AppBundle\Importer\JsonParser;
use HadesArchitect\JsonSchemaBundle\Validator\ValidatorService;
use PHPUnit\Framework\TestCase;
use HadesArchitect\JsonSchemaBundle\Exception\JsonSchemaException;

class JsonParserTest extends TestCase
{
    public function testThrowsExceptionOnInvalidJson()
    {
        $validatorService = $this->createMock(ValidatorService::class);
        $jsonParser = new JsonParser($validatorService);
        $this->expectException(JsonSchemaException::class);
        $jsonParser->parse(__DIR__ . '/fixture/invalid.json');
    }

    public function testThrowsExceptionOnInvalidJsonSchema()
    {
        $validatorService = $this->createMock(ValidatorService::class);
        $jsonParser = new JsonParser($validatorService);
        $this->expectException(JsonSchemaException::class);
        $jsonParser->parse(__DIR__ . '/fixture/invalid-schema.json');
    }

    public function testParsesValidJson()
    {
        $validatorService = $this->createMock(ValidatorService::class);
        $jsonParser = new JsonParser($validatorService);
        $messages = $jsonParser->parse(__DIR__ . '/fixture/valid.json');
        $this->assertArrayHasKey(0, $messages);
        $this->assertEquals(get_class($messages[0]), Message::class);
    }
}
