<?php

namespace Tests\AppBundle\Importer;

use AppBundle\Entity\Message;
use AppBundle\Importer\ImportResult;
use AppBundle\Importer\JsonParser;
use HadesArchitect\JsonSchemaBundle\Validator\ValidatorService;
use PHPUnit\Framework\TestCase;
use HadesArchitect\JsonSchemaBundle\Exception\JsonSchemaException;

class ImportResultTest extends TestCase
{
    public function testReturnsImportedCount()
    {
        $importedCount = 5;
        $importResult = new ImportResult($importedCount);

        $this->assertEquals($importedCount, $importResult->getMessageCount());
    }
}
