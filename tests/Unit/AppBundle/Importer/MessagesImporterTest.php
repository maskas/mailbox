<?php

namespace Tests\AppBundle\Importer;

use AppBundle\Entity\Message;
use AppBundle\Importer\Exception\DatabaseException;
use AppBundle\Importer\ImportResult;
use AppBundle\Importer\JsonParser;
use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManager;
use PHPUnit\Framework\TestCase;
use AppBundle\Importer\MessagesImporter;
use Doctrine\ORM\Mapping\ClassMetadata;

class MessagesImporterTest extends TestCase
{

    public function testReturnCorrectDataOnSuccessfulImport()
    {
        $jsonFile = 'someJsonFile.json';
        $em = $this->createMock(EntityManager::class);
        $classMetadata = $this->createMock(ClassMetadata::class);
        $connection = $this->createMock(Connection::class);
        $jsonParser = $this->createMock(JsonParser::class);

        $message1 = $this->createMock(Message::class);
        $message2 = $this->createMock(Message::class);

        $parseResult = [$message1, $message2];

        $em->expects($this->once())
            ->method('getClassMetadata')
            ->with(Message::class)
            ->willReturn($classMetadata);


        $classMetadata->expects($this->once())
            ->method('setIdGeneratorType')
            ->with(ClassMetadata::GENERATOR_TYPE_NONE);

        $em->expects($this->once())
            ->method('getConnection')
            ->willReturn($connection);

        $connection->expects($this->once())
            ->method('beginTransaction');

        $connection->expects($this->once())
            ->method('commit');


        $jsonParser->expects($this->once())
            ->method('parse')
            ->with($jsonFile)
            ->willReturn($parseResult);
        $messagesImporter = new MessagesImporter($jsonParser, $em);

        $importResult = $messagesImporter->importFromJsonFile($jsonFile);

        $this->assertEquals(get_class($importResult), ImportResult::class);
        $this->assertEquals(count($parseResult), $importResult->getMessageCount());
    }

    public function testRollbackOnError()
    {
        $jsonFile = 'someJsonFile.json';
        $em = $this->createMock(EntityManager::class);
        $classMetadata = $this->createMock(ClassMetadata::class);
        $connection = $this->createMock(Connection::class);
        $jsonParser = $this->createMock(JsonParser::class);

        $message1 = $this->createMock(Message::class);
        $message2 = $this->createMock(Message::class);

        $parseResult = [$message1, $message2];

        $em->expects($this->once())
            ->method('getClassMetadata')
            ->with(Message::class)
            ->willReturn($classMetadata);

        $classMetadata->expects($this->once())
            ->method('setIdGeneratorType')
            ->with(ClassMetadata::GENERATOR_TYPE_NONE);

        $em->expects($this->once())
            ->method('getConnection')
            ->willReturn($connection);

        $connection->expects($this->once())
            ->method('beginTransaction');

        $connection->expects($this->once())
            ->method('rollback');

        $em->method('flush')->willThrowException(new \Exception());

        $jsonParser->expects($this->once())
            ->method('parse')
            ->with($jsonFile)
            ->willReturn($parseResult);
        $messagesImporter = new MessagesImporter($jsonParser, $em);

        $this->expectException(DatabaseException::class);

        $messagesImporter->importFromJsonFile($jsonFile);
    }
}
