<?php

namespace Tests;

use Doctrine\ORM\EntityManager;

class MailboxDbTestCase extends MailboxTestCase
{

    /** @var EntityManager */
    private $em;

    /**
     * Constructs a test case with the given name.
     *
     * @param string $name
     * @param array  $data
     * @param string $dataName
     */
    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $kernel = self::bootKernel();

        $this->em = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();

        
    }

    public function getEntityManager()
    {
        return $this->em;
    }
}
