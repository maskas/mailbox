<?php

namespace AppBundle\Controller;

use AppBundle\Repository\MessageRepository;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\View\View;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;


class MessageController extends FOSRestController
{
    /**
     * @Rest\Get("/api/message")
     * @Rest\View(serializerGroups={"not-archived"})
     *
     * @param Request $request
     *
     * @ApiDoc(
     *  description="Get paginated not archived messages",
     *  section="MailboxApi API",
     *  parameters={
     *      {"name"="page", "dataType"="integer", "required"=false, "description"="Page numbers. Starts at 1"},
     *      {"name"="limit", "dataType"="integer", "required"=false, "description"="Max results in one page"}
     *  },
     *  output={
     *      "class"="AppBundle\Entity\Message",
     *      "groups"={"not-archived"},
     *      "collection"=true
     *  }
     * )
     * @return array
     */
    public function getAction(Request $request)
    {
        $page = $request->get('page', 1);
        $limit = $request->get('limit', 3);
        $filter = [
            'isArchived' => false
        ];
        $messages = $this->getRepository()->findBy($filter, ['timeSent' => 'desc'], $limit, ($page - 1) * $limit);

        return $messages;
    }

    /**
     * @Rest\Get("/api/message-archived")
     * @Rest\View(serializerGroups={"archived"})     *
     *
     * @param Request $request
     *
     * @ApiDoc(
     *  description="Get paginated archived messages",
     *  section="MailboxApi API",
     *  parameters={
     *      {"name"="page", "dataType"="integer", "required"=false, "description"="Page numbers. Starts at 1"},
     *      {"name"="limit", "dataType"="integer", "required"=false, "description"="Max results in one page"}
     *  },
     *  output={
     *      "class"="AppBundle\Entity\Message",
     *      "groups"={"archived"},
     *      "collection"=true
     *  }
     * )
     *
     * @return array
     */
    public function getArchivedAction(Request $request)
    {
        $page = $request->get('page', 1);
        $limit = $request->get('limit', 3);
        $filter = [
            'isArchived' => true
        ];
        $messages = $this->getRepository()->findBy($filter, ['timeSent' => 'desc'],  $limit, ($page - 1) * $limit);
        return $messages;
    }


    /**
     * @Rest\Get("/api/message/{messageId}")
     * @Rest\View(serializerGroups={"full"})
     *
     * @param int $messageId
     *
     * @ApiDoc(
     *  description="Get message by id",
     *  section="MailboxApi API",
     *  requirements={
     *      {"name"="messageId", "dataType"="integer", "required"=true, "description"="Message id"},
     *  },
     *  output={
     *      "class"="AppBundle\Entity\Message",
     *      "groups"={"full"},
     *  }
     * )
     *
     * @return \AppBundle\Entity\Message|View|null
     */
    public function idAction($messageId)
    {
        $message = $this->getRepository()->find($messageId);
        if ($message === null) {
            return new View("message not found", Response::HTTP_NOT_FOUND);
        }

        return $message;
    }

    /**
     * @Rest\Patch("/api/message/{messageId}/archive")
     * @Rest\View(serializerGroups={"full"})
     *
     * @param int $messageId
     *
     * @ApiDoc(
     *  description="Archive message",
     *  section="MailboxApi API",
     *  requirements={
     *      {"name"="messageId", "dataType"="integer", "required"=true, "description"="Message id"},
     *  },
     *  output={
     *      "class"="AppBundle\Entity\Message",
     *      "groups"={"full"},
     *  }
     * )
     *
     * @return \AppBundle\Entity\Message|View|null
     */
    public function archiveAction($messageId)
    {
        $message = $this->getRepository()->find($messageId);
        if ($message === null) {
            return new View("message not found", Response::HTTP_NOT_FOUND);
        }

        $message->archive();

        $em = $this->getDoctrine()->getManager();
        $em->persist($message);
        $em->flush();

        return $message;
    }


    /**
     * @Rest\Patch("/api/message/{messageId}/read")
     * @Rest\View(serializerGroups={"full"})
     *
     * @param int $messageId
     *
     * @ApiDoc(
     *  description="Read message",
     *  section="MailboxApi API",
     *  requirements={
     *      {"name"="messageId", "dataType"="integer", "required"=true, "description"="Message id"},
     *  },
     *  output={
     *      "class"="AppBundle\Entity\Message",
     *      "groups"={"full"},
     *  }
     * )
     *
     * @return \AppBundle\Entity\Message|View|null
     */
    public function readAction($messageId)
    {
        $message = $this->getRepository()->find($messageId);
        if ($message === null) {
            return new View("message not found", Response::HTTP_NOT_FOUND);
        }

        $message->read();

        $em = $this->getDoctrine()->getManager();
        $em->persist($message);
        $em->flush();

        return $message;
    }

    /**
     * @return MessageRepository
     */
    private function getRepository()
    {
        return $this->getDoctrine()->getRepository('AppBundle:Message');
    }
}
