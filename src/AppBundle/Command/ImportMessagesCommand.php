<?php

namespace AppBundle\Command;

use AppBundle\Importer\Exception\ImporterException;
use AppBundle\Importer\MessagesImporter;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ImportMessagesCommand extends ContainerAwareCommand
{
    /**
     * @var MessagesImporter
     */
    private $messageImporter;

    /**
     * ImportMessagesCommand constructor.
     * @param MessagesImporter $messageImporter
     */
    public function __construct(MessagesImporter $messageImporter)
    {
        $this->messageImporter = $messageImporter;

        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setName('app:import-messages')
            ->setDescription('Imports messages from JSON file.')
            ->addArgument('messagesFile', InputArgument::REQUIRED, 'Path to the messages JSON file.')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln([
            'Messages import started',
        ]);

        $messagesJsonFile = $input->getArgument('messagesFile');
        try {
            $result = $this->messageImporter->importFromJsonFile($messagesJsonFile);

            $output->writeln([
                '<info>Messages have been successfully imported!</info>',
                sprintf('<info>Imported messages count: %d</info>', $result->getMessageCount())
            ]);

        } catch (ImporterException $e) {
            $output->writeln([
                '<error>An error has occurred. No messages were imported.</error>',
                '============',
                sprintf('<error>%s</error>', $e->getMessage())
            ]);

        }
    }
}
