<?php
/**
 * Created by PhpStorm.
 * User: Mangirdas Skripka
 * Date: 11/19/2017
 * Time: 11:09 PM
 */

namespace AppBundle\Importer;

class ImportResult
{
    /**
     * @var int
     */
    private $messageCount;

    /**
     * ImportResult constructor.
     * @param int $messageCount
     */
    public function __construct(int $messageCount)
    {
        $this->messageCount = $messageCount;
    }

    /**
     * @return int
     */
    public function getMessageCount(): int
    {
        return $this->messageCount;
    }
}
