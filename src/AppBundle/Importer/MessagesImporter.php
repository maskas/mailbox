<?php

namespace AppBundle\Importer;

use AppBundle\Entity\Message;
use AppBundle\Importer\Exception\DatabaseException;
use AppBundle\Importer\Exception\ImporterException;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping\ClassMetadata;

class MessagesImporter
{
    /**
     * @var JsonParser
     */
    private $jsonParser;
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * MessagesImporter constructor.
     * @param JsonParser $jsonParser
     * @param EntityManager $em
     */
    public function __construct(JsonParser $jsonParser, EntityManager $em)
    {
        $this->jsonParser = $jsonParser;
        $this->em = $em;
    }

    /**
     * @param string $jsonPath
     * @return ImportResult
     * @throws ImporterException
     */
    public function importFromJsonFile(string $jsonPath)
    {
        $messages = $this->jsonParser->parse($jsonPath);
        $metadata = $this->em->getClassMetadata(Message::class);
        $metadata->setIdGeneratorType(ClassMetadata::GENERATOR_TYPE_NONE);
        $connection = $this->em->getConnection();

        $connection->beginTransaction();
        try {
            foreach ($messages as $message) {
                $this->em->persist($message);
            }
            $this->em->flush();
            $connection->commit();
        } catch (\Exception $e) {
            $connection->rollBack();
            throw new DatabaseException($e->getMessage());
        }

        $totalMessages = count($messages);
        $result = new ImportResult($totalMessages);
        return $result;
    }
}
