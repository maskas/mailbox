<?php

namespace AppBundle\Importer\Exception;

class FileNotFoundException extends ImporterException
{
}
