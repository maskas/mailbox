<?php

namespace AppBundle\Importer\Exception;

class InvalidJsonException extends ImporterException
{
}
