<?php

namespace AppBundle\Importer\Exception;

class DatabaseException extends ImporterException
{
}
