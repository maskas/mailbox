<?php
/**
 * Created by PhpStorm.
 * User: Mangirdas Skripka
 * Date: 11/18/2017
 * Time: 4:29 PM
 */

namespace AppBundle\Importer;

use AppBundle\Importer\Exception\FileNotFoundException;
use HadesArchitect\JsonSchemaBundle\Exception\JsonSchemaException;
use HadesArchitect\JsonSchemaBundle\Exception\ViolationException;
use HadesArchitect\JsonSchemaBundle\Validator\ValidatorService;
use AppBundle\Entity\Message;
use AppBundle\Importer\Exception\InvalidJsonException;

class JsonParser
{
    /**
     * @var ValidatorService
     */
    private $validatorService;

    /**
     * JsonEmailParser constructor.
     * @param ValidatorService $validatorService
     */
    public function __construct(ValidatorService $validatorService)
    {
        $this->validatorService = $validatorService;
    }

    /**
     * @param string $jsonPath
     * @return Message[]
     * @throws FileNotFoundException
     */
    public function parse(string $jsonPath)
    {
        if (!is_file($jsonPath)) {
            throw new FileNotFoundException(sprintf("File doesn't exist: %s", $jsonPath));
        }
        $json = file_get_contents($jsonPath);

        $data = json_decode($json);

        if (is_null($data)) {
            throw new JsonSchemaException("Invalid JSON file.");
        }

        try {
            $this->validatorService->check($data, $this->messagesJsonSchema());
        } catch (ViolationException $e) {
            throw new JsonSchemaException($e->getMessage());
        }

        $messages = [];
        foreach ($data->messages as $message) {
            $messages[] = $this->buildEmail($message);
        }

        return $messages;
    }

    /**
     * @param Object $emailData
     * @return Message
     */
    private function buildEmail($emailData)
    {
        $message = new Message(
            $emailData->sender,
            $emailData->subject,
            $emailData->message,
            (int) $emailData->time_sent
        );
        $message->setId($emailData->uid);
        return $message;
    }

    private function messagesJsonSchema()
    {
        return (object)[
            "\$schema" => "http://json-schema.org/draft-04/schema#",
            "title" => "Messages",
            "description" => "Messages to be imported",
            "type" => "object",
            "properties" => [
                "messages" => (object)[
                    "description" => "An array of email messages",
                    "type" => "array",
                    "items" => [
                        "type" => "object",
                        "properties" => [
                            "uid" => (object)[
                                "description" => "Unique ID of the messages",
                                "type" => "string"
                            ],
                            "sender" => (object)[
                                "description" => "Full name of the sender",
                                "type" => "string"
                            ],
                            "subject" => (object)[
                                "description" => "Email subject",
                                "type" => "string"
                            ],
                            "message" => (object)[
                                "description" => "Email message",
                                "type" => "string"
                            ],
                            "time_sent" => (object)[
                                "description" => "Unix timestamp",
                                "type" => "integer"
                            ]
                        ],
                        "required" => ["uid", "sender", "subject", "message", "time_sent"]
                    ]
                ]
            ],
            "required" => ["messages"]
        ];
    }
}
