<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Groups;

/**
 * Message
 *
 * @ORM\Table(name="message")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\MessageRepository")
 */
class Message
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")

     * @Groups({"full", "not-archived", "archived"})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="sender", type="string", length=255)
     *
     * @Groups({"full", "not-archived", "archived"})
     */
    private $sender;

    /**
     * @var string
     *
     * @ORM\Column(name="subject", type="string", length=255)
     *
     * @Groups({"full", "not-archived", "archived"})
     *
     */
    private $subject;

    /**
     * @var string
     *
     * @ORM\Column(name="message", type="text")
     *
     * @Groups({"full", "not-archived", "archived"})
     */
    private $message;

    /**
     * @var int
     *
     * @ORM\Column(name="time_sent", type="bigint")
     *
     * @Groups({"full", "not-archived", "archived"})
     */
    private $timeSent;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_archived", type="boolean")
     *
     * @Groups({"full"})
     */
    private $isArchived;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_read", type="boolean")
     *
     * @Groups({"full", "not-archived", "archived"})
     */
    private $isRead;

    /**
     * Email constructor.
     * @param string $sender
     * @param string $subject
     * @param string $message
     * @param int $timeSent
     */
    public function __construct(
        $sender,
        $subject,
        $message,
        $timeSent
    ) {
        $this->sender = $sender;
        $this->subject = $subject;
        $this->message = $message;
        $this->timeSent = $timeSent;
        $this->isArchived = false;
        $this->isRead = false;
    }

    /**
     * @param $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get sender
     *
     * @return string
     */
    public function getSender()
    {
        return $this->sender;
    }

    /**
     * Get subject
     *
     * @return string
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * Get message
     *
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * Get timeSent
     *
     * @return int
     */
    public function getTimeSent()
    {
        return $this->timeSent;
    }

    /**
     * Get isArchived
     *
     * @return bool
     */
    public function getIsArchived()
    {
        return $this->isArchived;
    }

    /**
     * Get isRead
     *
     * @return bool
     */
    public function getIsRead()
    {
        return $this->isRead;
    }

    public function archive()
    {
        $this->isArchived = true;
    }

    public function read()
    {
        $this->isRead = true;
    }
}
